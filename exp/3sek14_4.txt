+----------------------------------------------------------+
| FACILITY Teta u stroje 01                                |
+----------------------------------------------------------+
|  Status = BUSY                                           |
|  Time interval = 0 - 345600                              |
|  Number of requests = 20144                              |
|  Average utilization = 0.990842                          |
+----------------------------------------------------------+

+----------------------------------------------------------+
| FACILITY Teta u stroje 02                                |
+----------------------------------------------------------+
|  Status = BUSY                                           |
|  Time interval = 0 - 345600                              |
|  Number of requests = 20116                              |
|  Average utilization = 0.989453                          |
+----------------------------------------------------------+

+----------------------------------------------------------+
| FACILITY Teta u stroje 03                                |
+----------------------------------------------------------+
|  Status = BUSY                                           |
|  Time interval = 0 - 345600                              |
|  Number of requests = 20087                              |
|  Average utilization = 0.988067                          |
+----------------------------------------------------------+

+----------------------------------------------------------+
| FACILITY Pripravkar                                      |
+----------------------------------------------------------+
|  Status = not BUSY                                       |
|  Time interval = 0 - 345600                              |
|  Number of requests = 270                                |
|  Average utilization = 0.375                             |
+----------------------------------------------------------+

+----------------------------------------------------------+
| FACILITY Kotouc                                          |
+----------------------------------------------------------+
|  Status = not BUSY                                       |
|  Time interval = 0 - 345600                              |
|  Number of requests = 270                                |
|  Average utilization = 0.140625                          |
+----------------------------------------------------------+

+----------------------------------------------------------+
| QUEUE Fronta na Tetu A                                   |
+----------------------------------------------------------+
|  Time interval = 0 - 345600                              |
|  Incoming  20070                                         |
|  Outcoming  20054                                        |
|  Current length = 16                                     |
|  Maximal length = 27                                     |
|  Average length = 13.025                                 |
|  Minimal time = 2                                        |
|  Maximal time = 448                                      |
|  Average time = 224.376                                  |
|  Standard deviation = 129.308                            |
+----------------------------------------------------------+
+----------------------------------------------------------+
| QUEUE Fronta na Tetu B                                   |
+----------------------------------------------------------+
|  Time interval = 0 - 345600                              |
|  Incoming  20038                                         |
|  Outcoming  20026                                        |
|  Current length = 12                                     |
|  Maximal length = 27                                     |
|  Average length = 13.0057                                |
|  Minimal time = 2                                        |
|  Maximal time = 448                                      |
|  Average time = 224.398                                  |
|  Standard deviation = 129.395                            |
+----------------------------------------------------------+
+----------------------------------------------------------+
| QUEUE Fronta na Tetu C                                   |
+----------------------------------------------------------+
|  Time interval = 0 - 345600                              |
|  Incoming  20011                                         |
|  Outcoming  19997                                        |
|  Current length = 14                                     |
|  Maximal length = 40                                     |
|  Average length = 19.4889                                |
|  Minimal time = 3                                        |
|  Maximal time = 672                                      |
|  Average time = 336.754                                  |
|  Standard deviation = 194.187                            |
+----------------------------------------------------------+
+----------------------------------------------------------+
| HISTOGRAM Validace                                       |
+----------------------------------------------------------+
| STATISTIC                                                |
+----------------------------------------------------------+
|  Min = 2                       Max = 180                 |
|  Number of records = 6704                                |
|  Average value = 49.9007                                 |
|  Standard deviation = 7.47432                            |
+----------------------------------------------------------+
|    from    |     to     |     n    |   rel    |   sum    |
+------------+------------+----------+----------+----------+
|      0.000 |     10.000 |       91 | 0.013574 | 0.013574 |
|     10.000 |     20.000 |       89 | 0.013276 | 0.026850 |
|     20.000 |     30.000 |        0 | 0.000000 | 0.026850 |
|     30.000 |     40.000 |       89 | 0.013276 | 0.040125 |
|     40.000 |     50.000 |        0 | 0.000000 | 0.040125 |
|     50.000 |     60.000 |     6428 | 0.958831 | 0.998956 |
|     60.000 |     70.000 |        0 | 0.000000 | 0.998956 |
|     70.000 |     80.000 |        2 | 0.000298 | 0.999254 |
|     80.000 |     90.000 |        3 | 0.000447 | 0.999702 |
|     90.000 |    100.000 |        0 | 0.000000 | 0.999702 |
|    100.000 |    110.000 |        0 | 0.000000 | 0.999702 |
|    110.000 |    120.000 |        0 | 0.000000 | 0.999702 |
|    120.000 |    130.000 |        0 | 0.000000 | 0.999702 |
|    130.000 |    140.000 |        0 | 0.000000 | 0.999702 |
|    140.000 |    150.000 |        0 | 0.000000 | 0.999702 |
|    150.000 |    160.000 |        1 | 0.000149 | 0.999851 |
|    160.000 |    170.000 |        0 | 0.000000 | 0.999851 |
|    170.000 |    180.000 |        0 | 0.000000 | 0.999851 |
|    180.000 |    190.000 |        1 | 0.000149 | 1.000000 |
|    190.000 |    200.000 |        0 | 0.000000 | 1.000000 |
+------------+------------+----------+----------+----------+

