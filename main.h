#ifndef MAIN_H
#define MAIN_H

#include "simlib.h"
#include <getopt.h>
#include <cstdlib>
#include <string>

#define SEKUNDA 1.0
#define MINUTA 60 * SEKUNDA
#define HODINA 60 * MINUTA
#define DEN 24 * HODINA

double DOVEZENI_KOTOUCE = 3 * MINUTA;
double VYMENA_KOTOUCE = 5 * MINUTA;
double PORUCHA = DEN;
double PORUCHA_DOBA = 10 * MINUTA;
double UDRZBA = 30 * DEN;
double UDRZBA_DOBA = 8 * HODINA;
double SEKANI = 1.5 * SEKUNDA;
/******/double SEKANI3 = 1.2 * SEKUNDA; //---------------------
double TETA_BALENI = 17 * SEKUNDA;    //rychlost akou teta zabali servitky do baliku
/**/double TETA_BALENI3 = 13 * SEKUNDA;//---------------------
double BALIK_CESTA = 12 * SEKUNDA;

int KUSY_Z_KOTOUCE = 112500;    //112500 137500  150000  225000
int KUSY_V_BALIKU = 500;
int KUSY_NA_PODLAZKU = 9;

// pro validaci
double cas_kotuca;
// Doba behu programu
int DOBA_BEHU = 345600; // 4 dny
// Pocet stroju
int POCET_STROJU = 2;
// Beh programu bude probihat i s poruchami
bool PORUCHY_ZAPNUTY = false;
// validace 
bool VALIDACE_ZAPNUTA = false;


//Pripravar
Facility Pripravar("Pripravkar");
// Facility Pripravar2("Pripravkar2");
Queue qPripravar("Fronta na pripravare");
// Queue qPripravar2("Fronta na pripravare2");

// Store Pripravari("Pripravari", 3);



Facility Zamestnankyne_A("Teta u stroje 01");
Facility Zamestnankyne_B("Teta u stroje 02");
Facility Zamestnankyne_C("Teta u stroje 03");
Queue qZamestnankyne_A("Fronta na Tetu A");
Queue qZamestnankyne_B("Fronta na Tetu B");
Queue qZamestnankyne_C("Fronta na Tetu C");

// v petriho siti mame dve mista s koutoucem,
// ale vyseldek bude stejny kdyz bude jedno mist
Facility Kotouc("Kotouc");

Facility Stroj01_Stop("Stroj 01 - Stoji");
Facility Stroj02_Stop("Stroj 02 - Stoji");
Facility Stroj03_Stop("Stroj 03 - Stoji");

Facility Stroj01_Continue("Stroj 01 - Muze pokracovat");
Facility Stroj02_Continue("Stroj 02 - Muze pokracovat");
Facility Stroj03_Continue("Stroj 03 - Muze pokracovat");

Facility Stroj01_Work("Stroj 01 - Striha");
Facility Stroj02_Work("Stroj 02 - Striha");
Facility Stroj03_Work("Stroj 03 - Striha");

Facility Stroj01_Empty("Stroj 01 - Bude prazdny");
Facility Stroj02_Empty("Stroj 02 - Bude prazdny");
Facility Stroj03_Empty("Stroj 03 - Bude prazdny");

Store Stroj01_Stack("Stroj 01 - narezane kusy z jednoho kotouce", KUSY_V_BALIKU);
Store Stroj02_Stack("Stroj 02 - narezane kusy z jednoho kotouce", KUSY_V_BALIKU);
Store Stroj03_Stack("Stroj 03 - narezane kusy z jednoho kotouce", KUSY_V_BALIKU);

Store Stroj01_Pieses("Stroj 01 - narezane kusy celkem z jednoho baliku", KUSY_Z_KOTOUCE);
Store Stroj02_Pieses("Stroj 02 - narezane kusy celkem z jednoho baliku", KUSY_Z_KOTOUCE);
Store Stroj03_Pieses("Stroj 03 - narezane kusy celkem z jednoho baliku", KUSY_Z_KOTOUCE);

Store Flooring("Podlážka", KUSY_NA_PODLAZKU);

Histogram validace("Validace", 0,10, 20);

#endif /* MAIN_H */