#include "main.h"
#include <stdlib.h>

using namespace std;

class Stroj01_Porucha : public Process
{
public:
    double time;

    void Behavior()
    {
        time = Time;
        Priority = 2;

        Seize(Stroj01_Continue);
        Seize(Pripravar);
        Wait(PORUCHA_DOBA);
        Release(Pripravar);
        Release(Stroj01_Continue);
    }
};

class Stroj02_Porucha : public Process
{
public:
    double time;

    void Behavior()
    {
        time = Time;
        Priority = 2;

        Seize(Stroj02_Continue);
        Seize(Pripravar);
        Wait(PORUCHA_DOBA);
        Release(Pripravar);
        Release(Stroj02_Continue);
    }
};

class GeneratorPorucha01 : public Event
{

    void Behavior()
    {
        (new Stroj01_Porucha)->Activate();
        Activate(Time + Exponential(PORUCHA));
    }
};

class GeneratorPorucha02 : public Event
{

    void Behavior()
    {
        (new Stroj02_Porucha)->Activate();
        Activate(Time + Exponential(PORUCHA));
    }
};

class PlnaKrabica : public Process
{

    void Behavior()
    {
        Leave(Flooring, 9);
    }
};

class Balik : public Process
{
public:
    double time;

    void Behavior()
    {
        time = Time;

        Wait(BALIK_CESTA);
        Enter(Flooring, 1);
        if (Flooring.Full()) {
            (new PlnaKrabica)->Activate();
            validace(Time - cas_kotuca);
            cas_kotuca = Time;
        }
    }
};

class Baleni01 : public Process
{

    void Behavior()
    {
        Leave(Stroj01_Stack, KUSY_V_BALIKU);

        if (Zamestnankyne_A.Busy() || qZamestnankyne_A.Length() != 0) {
            qZamestnankyne_A.Insert(this);
            Passivate();
        }
        Seize(Zamestnankyne_A);
        Wait(TETA_BALENI);
        Release(Zamestnankyne_A);

        if (qZamestnankyne_A.Length() != 0) qZamestnankyne_A.GetFirst()->Activate();

        (new Balik)->Activate();
    }
};

class Baleni02 : public Process
{

    void Behavior()
    {
        //time = Time;

        Leave(Stroj02_Stack, KUSY_V_BALIKU);

        if (Zamestnankyne_B.Busy() || qZamestnankyne_B.Length() != 0) {
            qZamestnankyne_B.Insert(this);
            Passivate();
        }

        Seize(Zamestnankyne_B);
        Wait(TETA_BALENI);
        Release(Zamestnankyne_B);

        if (qZamestnankyne_B.Length() != 0) qZamestnankyne_B.GetFirst()->Activate();

        (new Balik)->Activate();
    }
};

class Baleni03 : public Process
{

    void Behavior()
    {
        //time = Time;

        Leave(Stroj03_Stack, KUSY_V_BALIKU);

        if (Zamestnankyne_C.Busy() || qZamestnankyne_C.Length() != 0) {
            qZamestnankyne_C.Insert(this);
            Passivate();
        }

        Seize(Zamestnankyne_C);
        Wait(TETA_BALENI3);
        Release(Zamestnankyne_C);

        if (qZamestnankyne_C.Length() != 0) qZamestnankyne_C.GetFirst()->Activate();

        (new Balik)->Activate();
    }
};

/*

Prva linka

 */
class Stroj01 : public Process
{
public:
    double time;

    void Behavior()
    {
stroj01_start:
        Seize(Stroj01_Empty);
        if (Pripravar.Busy() || qPripravar.Length() != 0) {
            qPripravar.Insert(this);
            Passivate();
        }

        Seize(Pripravar);
        // Enter(Pripravari,1);
        Seize(Kotouc);
        Wait(DOVEZENI_KOTOUCE);
        Release(Kotouc);

        Seize(Stroj01_Stop);
        Wait(VYMENA_KOTOUCE);
        time = Time;
        Release(Pripravar);
        // Leave(Pripravari,1);

        if (qPripravar.Length() != 0) (qPripravar.GetFirst())->Activate();

        cas_kotuca = Time;

stroj01_sekani:
        if (!Stroj01_Pieses.Full()) {
            Seize(Stroj01_Continue);
            Wait(SEKANI);
            Release(Stroj01_Continue);
            Enter(Stroj01_Pieses, 50);
            Enter(Stroj01_Stack, 50);
            if (Stroj01_Stack.Full()) {
                (new Baleni01)->Activate();
            }
            goto stroj01_sekani;
        } else {
            Leave(Stroj01_Pieses, KUSY_Z_KOTOUCE);
            Release(Stroj01_Empty);
            Release(Stroj01_Stop);
            if (VALIDACE_ZAPNUTA == false) {
                goto stroj01_start;
            }
        }
    }
};

/*

Druha linka

 */
class Stroj02 : public Process
{
public:
    double time;

    void Behavior()
    {
        //time = Time;

stroj02_start:
        Seize(Stroj02_Empty);
        if (Pripravar.Busy() || qPripravar.Length() != 0) {
            qPripravar.Insert(this);
            Passivate();
        }
        Seize(Pripravar);
        // Enter(Pripravari,1);
        Seize(Kotouc);
        Wait(DOVEZENI_KOTOUCE);
        Release(Kotouc);

        Seize(Stroj02_Stop);
        Wait(VYMENA_KOTOUCE);
        time = Time;
        Release(Pripravar);
        // Leave(Pripravari,1);

        if (qPripravar.Length() != 0) (qPripravar.GetFirst())->Activate();

        cas_kotuca = Time;
stroj02_sekani:
        if (!Stroj02_Pieses.Full()) {
            Seize(Stroj02_Continue);
            Wait(SEKANI);
            Release(Stroj02_Continue);
            Enter(Stroj02_Pieses, 50);
            Enter(Stroj02_Stack, 50);
            if (Stroj02_Stack.Full()) {
                (new Baleni02)->Activate();
            }
            goto stroj02_sekani;
        } else {
            Leave(Stroj02_Pieses, KUSY_Z_KOTOUCE);
            Release(Stroj02_Empty);
            Release(Stroj02_Stop);
            goto stroj02_start;
        }
    }
};

/*

Tretia linka

 */
class Stroj03 : public Process
{
public:
    double time;

    void Behavior()
    {
        //time = Time;

stroj03_start:
        Seize(Stroj03_Empty);
        if (Pripravar.Busy() || qPripravar.Length() != 0) {
            qPripravar.Insert(this);
            Passivate();
        }
        Seize(Pripravar);
        // Enter(Pripravari,1);
        Seize(Kotouc);
        Wait(DOVEZENI_KOTOUCE);
        // Release(Kotouc);

        Seize(Stroj03_Stop);
        Wait(VYMENA_KOTOUCE);
        time = Time;
        Release(Pripravar);
        // Leave(Pripravari,1);

        if (qPripravar.Length() != 0) (qPripravar.GetFirst())->Activate();

        cas_kotuca = Time;
stroj03_sekani:
        if (!Stroj03_Pieses.Full()) {
            Seize(Stroj03_Continue);
            Wait(SEKANI3);
            Release(Stroj03_Continue);
            Enter(Stroj03_Pieses, 50);
            Enter(Stroj03_Stack, 50);
            if (Stroj03_Stack.Full()) {
                (new Baleni03)->Activate();
            }
            goto stroj03_sekani;
        } else {
            Leave(Stroj03_Pieses, KUSY_Z_KOTOUCE);
            Release(Stroj03_Empty);
            Release(Stroj03_Stop);
            goto stroj03_start;
        }
    }
};

void help()
{
    Print("Parametry:\n"
            "\t-s POCET_STROJU (1-3, default 2)\n"
            "\t-t DOBA_BEHU (default 345600s)\n"
            "\t-v (Validace)\n"
            "\t-p (Zapne poruchy u stroju. ale pouze u stroje 1 a 2)\n"
            "\t-h (Vypise napovedu)\n");
}

int main(int argc, char** argv)
{
    int c;
    while ((c = getopt(argc, argv, "s:t:phv")) != -1) {
        switch (c) {
            case 's':
                POCET_STROJU = atoi(optarg);
                if (POCET_STROJU > 3 || POCET_STROJU < 1) {
                    Print("Spatny pocet bezicich stroju");
                    return (EXIT_FAILURE);
                }
                break;
            case 'v':
                VALIDACE_ZAPNUTA = true;
                break;
            case 'p':
                PORUCHY_ZAPNUTY = true;
                break;
            case 't':
                DOBA_BEHU = atoi(optarg);
                break;
            case 'h':
                help();
                return (EXIT_SUCCESS);
                break;
            default:
                Print("Spatne zadane parametry na vstupu programu");
                return (EXIT_FAILURE);
                break;
        }
    }

    if (VALIDACE_ZAPNUTA == true) {
        POCET_STROJU = 1;
        PORUCHY_ZAPNUTY = true;
    } else if (PORUCHY_ZAPNUTY == true) {
        if (POCET_STROJU > 2) POCET_STROJU--;
    }

    // Zajisteni generovani zcela nahodnych cisel
    RandomSeed(time(NULL));

    Init(0, DOBA_BEHU);

    (new Stroj01)->Activate();
    if (POCET_STROJU > 1) {
        (new Stroj02)->Activate();
    }
    if (POCET_STROJU > 2) {
        (new Stroj03)->Activate();
    }

    if (PORUCHY_ZAPNUTY == true) {
        (new GeneratorPorucha01)->Activate(Time + Exponential(PORUCHA));
        if (POCET_STROJU > 1) {
            (new GeneratorPorucha02)->Activate(Time + Exponential(PORUCHA));
        }
    }

    Run();

    Zamestnankyne_A.Output();
    qZamestnankyne_A.Output();

    if (POCET_STROJU > 1)
        Zamestnankyne_B.Output();
    if (POCET_STROJU > 1)
        qZamestnankyne_B.Output();

    if (POCET_STROJU > 2)
        Zamestnankyne_C.Output();
    if (POCET_STROJU > 2)
        qZamestnankyne_C.Output();

    if (VALIDACE_ZAPNUTA == true)
        validace.Output();

    if (PORUCHY_ZAPNUTY == true) {
        Stroj01_Continue.Q1->SetName("Stroj01 - Poruchy");
        Stroj01_Continue.Q1->Output();
        if (POCET_STROJU > 1) {
            Stroj02_Continue.Q1->SetName("Stroj02 - Poruchy");
            Stroj02_Continue.Q1->Output();
        }
    }

    return (EXIT_SUCCESS);
}