all:
	g++ -g -O2 main.cc -lsimlib -lm -o main
validate:
	./main -v
run:
	./main -p -s 2
pack: clean
	zip 01_xblask02_xcerny63 *.h *.cc Makefile *.pdf
clean:
	rm -f *.o ./main *.zip
unpack: 
	unzip 01_xblask02_xcerny63.zip